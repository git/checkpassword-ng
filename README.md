# checkpassword-ng - Uniform password checking interface for applications
```
Copyright © 2021-2022 checkpassword-ng Authors <https://hacktivis.me/git/checkpassword-ng>
SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only
```

This is a clean-room reimplementation of D. J. Bernstein [checkpassword](https://cr.yp.to/checkpwd.html) using <https://cr.yp.to/checkpwd/interface.html> as documentation.
- Licensing of the original is USA Public Domain, which isn't enough of a license for EU contributions
- The original last version dates back to 2000 and distributions have been adding patches on top of it

## Sign your work - the Developer's Certificate of Origin

To make sure that everyone has proper rights on the code they are submitting, every contributor will sign-off their commits under the ` Developer's Certificate of Origin 1.1` (copy included in `DCO-1.1.txt`).

Pseudonyms are tolerated but it is highly recommended to use accountable names.
