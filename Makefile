# POSIX-ish Makefile with extensions common to *BSD and GNU such as:
# - Usage of backticks for shell evaluation
# - Usage of ?= for defining variables when not already defined
# - Usage of += for appending to a variable

VERSION = 0.1.0
VERSION_FULL = $(VERSION)`./version.sh`

PREFIX  = /usr/local
BINDIR  = $(PREFIX)/bin
LIBDIR  = $(PREFIX)/lib
MANDIR  = $(PREFIX)/share/man

CC        ?= cc
AR        ?= ar
EXE_CFLAGS = -pie -fPIE
LIB_CFLAGS = -fpic -fPIC
CFLAGS    ?= -g -O2 -fstack-protector-strong -D_FORTIFY_SOURCE=2 -Wall -Wextra -Wconversion -Wsign-conversion -O2 -Werror=implicit-function-declaration -Werror=implicit-int -Werror=vla

LIBS = -lcrypt

EXE   = checkpassword
LIBSO = libchkpw.so
LIBA  = libchkpw.a
SRC   = checkpassword.c chkpw.c

all: $(EXE) $(LIBSO) $(LIBA)

checkpassword: checkpassword.c chkpw.o
	$(CC) -std=c99 $(EXE_CFLAGS) $(CFLAGS) -o $@ $< chkpw.o $(LIBS) $(LDFLAGS)

libchkpw.so: chkpw.c
	$(CC) -std=c99 -c -shared $(LIB_CFLAGS) $(CFLAGS) -o $@ $<

chkpw.o: chkpw.c
	$(CC) -std=c99 -c $(LIB_CFLAGS) $(CFLAGS) -o $@ $<

libchkpw.a: chkpw.o
	$(AR) -rv $@ $<

.PHONY: install
install: all
	mkdir -p $(DESTDIR)$(BINDIR)/
	cp -p $(EXE) $(DESTDIR)$(BINDIR)/$(EXE)
	mkdir -p $(DESTDIR)$(LIBDIR)/
	cp -p $(LIBSO) $(LIBA) $(DESTDIR)$(LIBDIR)/
	mkdir -p $(DESTDIR)$(MANDIR)/man3/
	cp -p chkpw.3 $(DESTDIR)$(MANDIR)/man3/
	mkdir -p $(DESTDIR)$(MANDIR)/man8/
	cp -p $(EXE).8 $(DESTDIR)$(MANDIR)/man8/

.PHONY: clean
clean:
	rm -fr $(EXE) chkpw.o $(LIBSO) $(LIBA)

format: *.c
	clang-format -style=file -assume-filename=.clang-format -i *.c
