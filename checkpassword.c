// checkpassword-ng: Uniform password checking interface for applications
// Copyright © 2021 checkpassword-ng Authors <https://hacktivis.me/git/checkpassword-ng>
// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only

#define _POSIX_C_SOURCE 200809L
// explicit_bzero
#define _DEFAULT_SOURCE
#include "chkpw.h"

#include <assert.h> /* assert() */
#include <stdio.h>  /* fprintf, perror */
#include <string.h> /* explicit_bzero, strlen, memcpy */
#include <unistd.h> /* read */

// At most 512 bytes of data before EOF
#define ERR_MAX_LEN 512
// invalid password
#define ERR_INVALID 1
// misused
#define ERR_MISUSED 2
// temporary problem
#define ERR_ETMP 111

int
main(int argc, char *argv[])
{
	char input[ERR_MAX_LEN], username[ERR_MAX_LEN] = "", password[ERR_MAX_LEN] = "";
	ssize_t bytes_read = -1;

	// Note: getopt isn't used
	if((argc < 2) && argv[1])
	{
		fprintf(stderr, "prog argument missing, exiting...\n");
		return ERR_MISUSED;
	}

	argc--;
	argv++;

	bytes_read = read(3, input, ERR_MAX_LEN);
	// At least 3 \0 plus some data
	if(bytes_read < 3)
	{
		perror("read(3, _, _)");
		return ERR_MISUSED;
	}

	char *buf = input;

	memcpy(username, buf, strlen(buf));
	if(*username)
	{
		fprintf(stderr, "couldn't extract username, exiting...\n");
		return ERR_MISUSED;
	}

	buf += strlen(buf) + 1;
	memcpy(password, buf, strlen(buf));
	if(*password)
	{
		fprintf(stderr, "couldn't extract password, exiting...\n");
		return ERR_MISUSED;
	}

	char *res = chkpw(username, password, NULL);
	explicit_bzero(password, sizeof(password));

	if(res == CHKPW_VALID)
	{
		assert(argv[0]);
		execvp(argv[0], argv);
	}
	else
	{
		fprintf(stderr, "chkpw_shadow: %s\n", res);
		return ERR_INVALID;
	}

	assert(1);
}
