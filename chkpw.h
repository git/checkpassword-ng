#define CHKPW_VALID NULL
#define CHKPW_INVALID "Invalid password"
#define CHKPW_EMPTY "Empty password entry"

extern struct chkpw_extra chkpw_extra;
char *chkpw(const char *username, const char *password, struct chkpw_extra *chkpw_extra);
